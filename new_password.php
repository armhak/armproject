<?php include('server.php'); ?>
<?php
if(!$_GET['token']){
	 header('location: enter_email.php');
}
$tok = $_GET['token'];
$sql = "SELECT email FROM password_resets WHERE token='$tok'";
// var_dump($db);die;
$results = mysqli_query($db, $sql);
// var_dump($results);die;
if($results->num_rows <= 0){
	header('location: login.php');
}
$email = mysqli_fetch_assoc($results);
$email = $email['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Password Reset</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<form class="login-form" action="new_password.php" method="post">
		<h2 class="form-title">New password</h2>
		<!-- form validation messages -->
		<?php include('errors.php'); ?>
		<input type="hidden" name="email" value="<?php echo $email; ?>">
		<div class="form-group">
			<label>New password</label>
			<input type="password" name="new_pass">
		</div>
		<div class="form-group">
			<label>Confirm new password</label>
			<input type="password" name="new_pass_c">
		</div>
		<div class="form-group">
			<button type="submit" name="new_password" class="btn">Submit</button>
		</div>
	</form>
</body>
</html>