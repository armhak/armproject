<?php 
	session_start();

	$name = "";
	$surname = "";
	$phone = "";
	$username = "";
	$email = "";
	$errors = array();

	//connect to the database
	$db = mysqli_connect('localhost', 'arm91_db', 'E&p6wOU6', 'arm91_db');

	//Register user
		//if the register button is clicked
	if (isset($_POST['register'])) {
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
		$password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
		$name = mysqli_real_escape_string($db, $_POST['name']);
		$surname = mysqli_real_escape_string($db, $_POST['surname']);
		$phone = mysqli_real_escape_string($db, $_POST['phone']);

		$sql_p = "SELECT * FROM users WHERE phone='$phone'";
		$sql_e = "SELECT * FROM users WHERE email='$email'";
		$res_e = mysqli_query($db, $sql_e) or die(mysqli_error($db));
		$res_p = mysqli_query($db, $sql_p) or die(mysqli_error($db));

		// if (mysqli_num_rows($res_e) > 0) {
		// 	$name_error = "Sorry! THIS EMAIL ALREADY TAKEN";
		// }elseif (mysqli_num_rows($res_p) > 0) {
		// 	$name_error = "Sorry! THIS PHONE ALREADY TAKEN";
		// }else

		//ensure that form fields are filled properly
		
		if (empty($username)) {
			array_push($errors, "Username is required");  // add error to errors array 
		}
		if (empty($email)) {
			array_push($errors, "Email is required");  
		}elseif (mysqli_num_rows($res_e) > 0) {
			array_push($errors, "Sorry! THIS EMAIL ALREADY TAKEN");
		}
		if (empty($password_1)) {
			array_push($errors, "Password is required");  
		}

		if ($password_1 != $password_2) {
			array_push($errors, "The two passwords do not match");
		}

		if (empty($name)) {
			array_push($errors, "Name is required");  
		}
		if (empty($surname)) {
			array_push($errors, "Surname is required");  
		}
		if (empty($phone)) {
			array_push($errors, "Phone is required");  
		}elseif (mysqli_num_rows($res_p) > 0) {
			array_push($errors, "Sorry! THIS PHONE ALREADY TAKEN");
		}

		// if there are not errors, save user to database
		if (count($errors) == 0) {
			$password = md5($password_1); // encrypt password before storing in database (security) 
			$sql = "INSERT INTO users (username, email, password, name, surname, phone)
						VALUES ('$username', '$email', '$password', '$name', '$surname', '$phone')";
			mysqli_query($db, $sql);
			$_SESSION['username'] = $username;
			$_SESSION['success'] = "You are already logged in";
			header('location: index.php');
		}
	}

	// LOGIN USER
	if (isset($_POST['login'])) {
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($username)) {
			array_push($errors, "Username is required");
		}
		if (empty($password)) {
		  	array_push($errors, "Password is required");
		}

		if (count($errors) == 0) {
		  	$password = md5($password);
		  	$query = "SELECT * FROM users WHERE username='$username' OR email='$username' AND password='$password'";
		  	$results = mysqli_query($db, $query);
		  	if (mysqli_num_rows($results) == 1) {
		  	  $_SESSION['username'] = $username;
		  	  $_SESSION['success'] = "You are now logged in";
		  	  header('location: index.php');
		  	}
		  	else {
		  		array_push($errors, "Wrong username/password combination");
		  	}
		}
	}

	/*
  Accept email of user whose password is to be reset
  Send email to user to reset their password
*/
if (isset($_POST['reset-password'])) {
	// $email = mysqli_real_escape_string($db, $_POST['email']);

	// ensure that the user exists on our system
	$em = $_POST['email'];
	$query = "SELECT email FROM users WHERE email='$em'";
	$results = mysqli_query($db, $query);

	
	if (empty($em)) {
	    array_push($errors, "Your email is required");
	}else if(mysqli_num_rows($results) <= 0) {
	    array_push($errors, "Sorry, no user exists on our system with that email");
	}

	$email = mysqli_fetch_assoc($results);
  		
  if (count($errors) == 0) {
  
  // generate a unique random token of length 100
  	$token = generateRandomString(100);
    // store token in the password-reset database table against the user's email
    $sql = "INSERT INTO password_resets (email, token) VALUES ('$em', '$token')";
    $results = mysqli_query($db, $sql);
    
    // Send email to user with the token in a link they can click on
    $to = $em;
    $url = "http://arm91.beget.tech/new_password.php?token=". $token;
    $subject = "Reset your password on examplesite.com";

    $headers = "From: " . strip_tags($_POST['req-email']) . "\r\n";
    $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
	$headers .= "CC: admin@example.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
    $msg = "Hi there, click on this <a href='" . $url . "'>link</a> to reset your password on our site";
    $msg = wordwrap($msg,70);
    mail($to, $subject, $msg, $headers);
    header('location: pending.php?email=' . $em);
  }else{
  	// header('location: enter_email.php');
  }
}

// ENTER A NEW PASSWORD
if (isset($_POST['new_password'])) {
  $new_pass = mysqli_real_escape_string($db, $_POST['new_pass']);
  $new_pass_c = mysqli_real_escape_string($db, $_POST['new_pass_c']);

  // Grab to token that came from the email link
  if (empty($new_pass) || empty($new_pass_c)) array_push($errors, "Password is required");
  if ($new_pass !== $new_pass_c) array_push($errors, "Password do not match");
  if (count($errors) == 0) {
    // select email address of user from the password_reset table 
    $email = $_POST['email'];
    if ($email) {
      $new_pass = md5($new_pass);
      $sql = "UPDATE users SET password='$new_pass' WHERE email='$email'";
      $results = mysqli_query($db, $sql);
      header('location: index.php');
    }
  }
}


	//Logout
	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header('location: login.php');
	}

	function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>