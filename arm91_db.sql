-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 20, 2018 at 04:22 PM
-- Server version: 5.7.21-20-beget-5.7.21-20-1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arm91_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Creation: Sep 19, 2018 at 12:41 PM
-- Last update: Sep 20, 2018 at 12:49 PM
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`) VALUES
(0, 'armhakobyan86@gmail.com', 'rvSU4cKOijUVK3d7M88f53jkQ7gN7H7ycZthcd6ux0ph4DpQMx5RApbrwreD9lcllFDxTJ1rKrJO48FQGLIhaUIHmXlvixREdub7');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Creation: Sep 05, 2018 at 12:18 PM
-- Last update: Sep 20, 2018 at 12:49 PM
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `phone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `name`, `surname`, `phone`) VALUES
(1, 'Am', NULL, '912ec803b2ce49e4a541068d495ab570', '', '', NULL),
(4, 'Am', NULL, '7ab6684faaf74beeccb73263c7030935', '', '', NULL),
(6, 'Tommy', NULL, '76419c58730d9f35de7ac538c2fd6737', '', '', NULL),
(9, 'Qwerty', NULL, '376c43878878ac04e05946ec1dd7a55f', '', '', NULL),
(10, 'Tommy', NULL, 'a152e841783914146e4bcd4f39100686', '', '', NULL),
(11, 'asdf', NULL, 'd8578edf8458ce06fbc5bb76a58c5ca4', '', '', NULL),
(12, 'pin', NULL, '15137ac9aa4a99ca503af92199223644', '', '', NULL),
(13, 'hija', NULL, '76419c58730d9f35de7ac538c2fd6737', '', '', NULL),
(16, 'qwerty', NULL, 'd8578edf8458ce06fbc5bb76a58c5ca4', '', '', NULL),
(17, 'qwerty', NULL, 'd8578edf8458ce06fbc5bb76a58c5ca4', '', '', NULL),
(18, 'qazxc', NULL, '1bba93f43f200abb809cafcd503d9147', '', '', NULL),
(19, 'qwentun', 'qwer@we.we', 'a152e841783914146e4bcd4f39100686', '', '', NULL),
(20, 'Mart', 'qwert@we.qw', '040b7cf4a55014e185813e0644502ea9', '', '', NULL),
(22, 'ram', 'rm@mnb.cv', 'a152e841783914146e4bcd4f39100686', 'Ram', 'Jack', '+36985'),
(23, 'Dson', 'zxcvb@zxcv.zx', 'a152e841783914146e4bcd4f39100686', 'Den', 'Son', '+36547896654'),
(26, 'Bob777', 'zxcvb@zxcv777.zx', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Bob', 'Pops', '+6974563214'),
(40, 'qweqwr', 'rm@mnbii.cv', '76419c58730d9f35de7ac538c2fd6737', 'Asdf', 'Zxcv', '+6589'),
(42, 'qweqwr', 'rm@mnbeee.cv', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Asdf', 'Zxcv', '+65899'),
(44, 'Am', 'armhakobyan86@gmail.com', '8ab939ef9d2e7f386a417c7e594bef1f', 'Arm', 'Yn', '+37499966686');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `email_2` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
