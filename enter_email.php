<?php include('server.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Password Reset</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<form class="login-form" action="enter_email.php" method="post">
		<h2 class="form-title">Reset password</h2>
		<!-- form validation messages -->
		<?php include('errors.php'); ?>
		<div class="input-group">
			<label>Please enter Your email address</label>
			<input type="email" name="email" >
		</div>
		<div class="input-group">
			<input type="submit" name="reset-password" class="btn" value="Submit">
		</div>
	</form>
</body>
</html>